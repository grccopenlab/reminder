Reminder is a C# WinForms application for sending appointment reminders to student (and professional) tutors.

Reminder currently pulls it's students, tutors, instructors, and course options from an Access database. The email message content is constructed from a set of template text files, and leverages GMail's SMTP service to send each message.