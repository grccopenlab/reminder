﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

using FluentData;

namespace Reminder.Data
{
    public class OpenLabContext : IDisposable
    {
        IDbProvider provider;
        private String PATH = ConfigurationManager.AppSettings["PATH"];


        public OpenLabContext()
        {
            provider = new FluentData.AccessProvider();
        }


        public IList<Subject> GetCourses()
        {
            var context = new DbContext().ConnectionStringName("OpenLab", provider);
            return context
                .Sql("SELECT ClassID, ClassDescription FROM tbl_classes")
                .QueryMany<Subject>()
                .ToList();
        }


        public IList<string> GetHours()
        {
            XDocument xd = XDocument.Load(String.Format(@"{0}\hoursofoperation.xml", PATH));

            var hrs = xd.Root.Elements("span");

            try
            {
                IList<String> hours = new List<String>();
                foreach (var h in hrs)
                {
                    hours.Add(h.Value);
                }

                return hours;
            }
            catch
            {
                throw (new Exception("Hours data file could not be parsed."));
            }
        }


        public IList<Instructor> GetInstructors()
        {
            var context = new DbContext().ConnectionStringName("OpenLab", provider);
            return context
                .Sql("SELECT Inst_FName AS FirstName, Inst_LName AS LastName FROM tbl_Instructors WHERE Inst_Current = true")
                .QueryMany<Instructor>()
                .ToList();
        }


        public IList<Student> GetStudents()
        {
            var context = new DbContext().ConnectionStringName("OpenLab", provider);
            return context
                .Sql("SELECT stud_FName AS FirstName, stud_LName AS LastName FROM tbl_Students WHERE Stud_CurrentSemester = true")
                .QueryMany<Student>()
                .ToList();
        }


        public IList<Tutor> GetTutors(string filter = "*")
        {
            var context = new DbContext().ConnectionStringName("OpenLab", provider);

            string query = "SELECT tbl_tutors.tut_LName AS LastName, tbl_tutors.tut_FName AS FirstName, tbl_tutors.tut_Email AS Email, " +
                        "tbl_tutors.tut_Nick AS NickName, tbl_tutorType.ttype_Description AS LabRole " +
                        "FROM tbl_tutors LEFT JOIN tbl_tutorType ON tbl_tutors.[Tutor Type] = tbl_tutorType.ttype_ID " +
                        "WHERE (tbl_tutors.tut_LName <> NULL) AND (tbl_tutors.tut_FName <> NULL) AND (tbl_tutors.tut_Email <> NULL) " +
                        "      AND (tbl_tutors.tut_CurrentSchedule = true) AND (tbl_tutors.tut_CurrentTutor = true) ";

            if (filter != "*")
            {
                string clause = filter.Equals("Student Workers") ? "AND tbl_tutorType.ttype_Description = 'Student Workers'" : "AND tbl_tutorType.ttype_Description <> 'Student Workers'";
                query += clause;
            }

            return context
                .Sql(query)
                .QueryMany<Tutor>()
                .ToList();
        }


        #region IDisposable Members

        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeAll)
        {
            if (disposeAll)
            {
            }
        }

        #endregion
    }
}
