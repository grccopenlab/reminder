﻿using System;

namespace Reminder.Data
{
    public partial class Course : IComparable<Course>
    {
        public Course()
            :this(string.Empty, string.Empty, string.Empty)
        {
        }

        public Course(string code, string name, string instructorName)
        {
            this.Code = code;
            this.Name = name;
            this.Instructor = instructorName;
        }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Instructor { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1} - {2}", this.Code, this.Name, this.Instructor);
        }

        #region IComparable<Course> Members

        int IComparable<Course>.CompareTo(Course other)
        {
            return this.ToString().CompareTo(other.ToString());
        }

        #endregion
    }
}
