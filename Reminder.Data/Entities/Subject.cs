﻿using System;

namespace Reminder.Data
{
    public partial class Subject: IComparable<Subject>
    {
        public string ClassID { get; set; }

        public string ClassDescription { get; set; }

        public override string ToString()
        {
            return String.Format("{0}: {1}", ClassID, ClassDescription);
        }

        #region IComparable<Subject> Members

        int IComparable<Subject>.CompareTo(Subject other)
        {
            return this.ClassID.CompareTo(other.ClassID);
        }

        #endregion
    }
}
