﻿using System;
using System.Collections.Generic;

namespace Reminder.Data
{
    /// <summary>
    /// Class to hold relevent data for tutors
    /// </summary>
    public partial class Tutor
    {
        #region Members

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string NickName { get; set; }
        public string LabRole { get; set; }
        public string ID { get; set; }

        #endregion

        /// <summary>
        /// Custom format for the ToString() method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }
    }
}
