﻿using System.Configuration;

namespace Reminder.Desktop
{
    public class AppSettings
    {
        public static string ReturnEmail
        {
            get
            {
                return Find("RETURN_EMAIL");
            }
        }

        public static string UserName
        {
            get
            {
                return Find("USER_NAME");
            }
        }

        public static string Password
        {
            get
            {
                return Find("PASSWORD");
            }
        }

        public static string Find(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? string.Empty;
        }
    }
}
