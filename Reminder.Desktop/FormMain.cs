﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using Reminder.Data;

namespace Reminder.Desktop
{
    public partial class FormMain : Form
    {
        private string filter = "*";
        public FormMain()
        {
            InitializeComponent();

            this.Text = this.ProductName;

            LoadData();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                btnSend.Enabled = false;

                var tutor = cmbTutor.SelectedItem as Tutor;

                if (tutor.Email != String.Empty)
                {
                    lblSendingStatus.Text = "Sending message...";
                    var ui = TaskScheduler.FromCurrentSynchronizationContext();
                    string messageBody = BuildHTMLMessage(tutor.NickName, cmbStudent.Text, cmbClass.Text, cmbInstructor.Text, dtpAppointmentDate.Value.ToLongDateString(), cmbAppointmentTime.Text);
                    Task.Run(() =>
                    {
                        GmailHelper message = new GmailHelper(AppSettings.ReturnEmail, string.Format("{0} <{1}>", tutor.ToString(), tutor.Email));
                        message.UserName = AppSettings.UserName;
                        message.Password = AppSettings.Password;

                        message.Subject = "Tutoring Appointment Alert";

                        //  Call helper method and assign results to GmailMessage object's Body property.
                        message.Body = messageBody;
                        message.Send();

                    })
                    .ContinueWith(task =>
                    {
                        if (task.Status == TaskStatus.RanToCompletion)
                        {
                            MessageBox.Show(this, "Email has been sent!", this.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            ResetForm();
                        }
                        else
                        {
                            MessageBox.Show(this, "Email was not sent. Please wait a moment and then try again.", this.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            lblSendingStatus.Text = "Email was not sent.";
                            btnSend.Enabled = IsValidEmail();
                        }
                    }, ui);
                }
                else
                {
                    MessageBox.Show("Please select a tutor before sending this email.");
                }
            }
            catch (TimeoutException)
            {
                //  Call helper method
                ExceptionMessenger(new TimeoutException("Application has timed out attempting to send email."));

                lblSendingStatus.Text = "Email was not sent.";
                btnSend.Enabled = IsValidEmail();
            }
            catch (Exception ex)
            {
                //  Call helper method
                ExceptionMessenger(ex);

                lblSendingStatus.Text = "Email was not sent.";
                btnSend.Enabled = IsValidEmail();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetForm();
            btnSend.Enabled = false;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            var tutor = cmbTutor.SelectedItem as Tutor;
            if (tutor != null)
            {
                rtbPreview.Text = BuildMessage(tutor.NickName, cmbStudent.Text, cmbClass.Text, cmbInstructor.Text, dtpAppointmentDate.Value.ToLongDateString(), cmbAppointmentTime.Text);
            }

            btnSend.Enabled = IsValidEmail();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadData();
            ResetForm();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TutorRadioButton_Click(object sender, EventArgs e)
        {
            RadioButton rdb = (RadioButton)sender;
            filter = rdb.Tag.ToString();
            using (var context = new OpenLabContext())
            {
                ReloadComboBox(cmbTutor, context.GetTutors(filter).ToArray<Tutor>());
            }
        }

        private void ComboBox_TextChanged(object sender, EventArgs e)
        {
            btnSend.Enabled = IsValidEmail();
        }


        #region Helper Methods

        /// <summary>
        /// Helper method to load a generic combobox.
        /// </summary>
        private void ReloadComboBox(ComboBox cmb, object[] theList)
        {
            cmb.Items.Clear();
            cmb.Items.AddRange(theList);
            cmb.ResetText();
        }

        /// <summary>
        /// Helper method to load dropdown fields.
        /// </summary>
        private void LoadData()
        {
            try
            {
                using (var context = new OpenLabContext())
                {
                    ReloadComboBox(cmbTutor, context.GetTutors(filter).ToArray<Tutor>());
                    ReloadComboBox(cmbStudent, context.GetStudents().ToArray<Student>());
                    ReloadComboBox(cmbInstructor, context.GetInstructors().ToArray<Instructor>());
                    ReloadComboBox(cmbClass, context.GetCourses().ToArray<Subject>());
                    ReloadComboBox(cmbAppointmentTime, context.GetHours().ToArray<string>());
                }
            }
            catch (Exception ex)
            {
                //  Call helper method
                ExceptionMessenger(ex);
            }
        }

        /// <summary>
        /// Helper method for resetting the relevent text fields
        /// </summary>
        private void ResetForm()
        {
            dtpAppointmentDate.ResetText();
            rtbPreview.ResetText();
            rdbAllTutors.Checked = true;
            lblSendingStatus.Text = "...";
            filter = "*";

            using (var context = new OpenLabContext())
            {
                ReloadComboBox(cmbTutor, context.GetTutors(filter).ToArray<Tutor>());
            }

            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox || ctrl is ComboBox)
                {
                    ctrl.ResetText();
                }
            }
        }


        /// <summary>
        /// Helper method for building the reminder email
        /// </summary>
        /// <returns>A String object containing formatted text.</returns>
        private String BuildMessage(string tutor, string student, string course, string instructor, string date, string time)
        {
            string template = string.Empty;
            string path = string.Format(@"{0}\{1}", AppSettings.Find("PATH"), @"appointment-template.txt");
            using (var reader = new StreamReader(path, Encoding.UTF8))
            {
                template = reader.ReadToEnd();
            }

            template = template.Replace("%tutor%", tutor);
            template = template.Replace("%student%", student);
            template = template.Replace("%subject%", course);
            template = template.Replace("%instructor%", instructor);
            template = template.Replace("%date%", date);
            template = template.Replace("%time%", time);

            return template;
        }

        /// <summary>
        /// Helper method for building the reminder email
        /// </summary>
        /// <returns>A String object containing HTML-formatted text.</returns>
        private String BuildHTMLMessage(string tutor, string student, string course, string instructor, string date, string time)
        {
            string template = string.Empty;
            string path = string.Format(@"{0}\{1}", AppSettings.Find("PATH"), @"appointment-template.html");
            using (var reader = new StreamReader(path, Encoding.UTF8))
            {
                template = reader.ReadToEnd();
            }

            template = template.Replace("%tutor%", tutor);
            template = template.Replace("%student%", student);
            template = template.Replace("%subject%", course);
            template = template.Replace("%instructor%", instructor);
            template = template.Replace("%date%", date);
            template = template.Replace("%time%", time);

            return template;
        }


        /// <summary>
        /// Helper method for triggering enabled state of the Send button
        /// </summary>
        /// <returns>true if all required fields are valid; false otherwise</returns>
        private bool IsValidEmail()
        {
            return cmbTutor.Text != string.Empty && cmbStudent.Text != string.Empty && cmbInstructor.Text != string.Empty && cmbInstructor.Text != string.Empty && cmbAppointmentTime.Text != string.Empty;
        }

        /// <summary>
        /// Helper method for displaying exception messages
        /// </summary>
        /// <param name="ex">The exception for which this event was called to handle</param>
        private void ExceptionMessenger(Exception ex)
        {
#if DEBUG
            // Debug/development version - print stack trace
            MessageBox.Show(
                String.Format("{0}\n\n{1}", ex.Message, ex.StackTrace),
                this.ProductName,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                );
#else
            // Live version - only show error message
            MessageBox.Show(
                ex.Message,
                this.ProductName,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
#endif
        }

        #endregion
    }
}
